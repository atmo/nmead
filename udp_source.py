#!/usr/bin/python3
import socket
import time

IP = 'localhost'
PORT = 5005
FILENAME = 'nmea_big.txt'


def main():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    while True:
        with open(FILENAME, "rU") as f:
            for line in f:
                resend = False
                while True:
                    try:
                        print(line)
                        s.sendto(line.encode(), (IP, PORT))
                    except socket.error:
                        resend = True
                    if not resend:
                        break

        print("reached EOFs")


if __name__ == '__main__':
    main()
