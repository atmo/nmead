#!/usr/bin/python3
import socket
import time
import pickle
import struct

import pynmea2_lib

IP = '127.0.0.1'
PORT = 5005

GRAPHITE_IP = '127.0.0.1'
GRAPHITE_PORT = 2004

BATCH_SEND_INTERVAL = 1

#  for i in 4 6 8 16 2; do echo "nmea.count $i `date +%s`" | nc -q0 127.0.0.1 2003; sleep 6; done

send_socket = socket.socket()
send_socket.connect((GRAPHITE_IP, GRAPHITE_PORT))

def send_batch(batch):
    payload = pickle.dumps(batch, protocol=2)
    print("Sending batch of {0} messages, {1} bytes length".format(len(batch), len(payload)))
    header = struct.pack("!L", len(payload))
    send_socket.sendall(header)
    send_socket.sendall(payload)

def main():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind((IP, PORT))
    file_obj = s.makefile()
    last_batch_ts = time.time()
    batch = []
    while True:
        for line in file_obj:
            try:
                print(line)
                sentence = pynmea2_lib.parse(line)
            except pynmea2_lib.ParseError as e:
                print(e)
                continue
            metric = sentence.__class__.__name__
            for f in sentence.fields:
                desc, name = f[0], f[1]
                path = "nmea.{metric}.{part}".format(metric=metric.lower(), part=name)
                value = getattr(sentence, name)
                try:
                    value = float(value)
                except Exception as e:
                    continue
                ts = int(time.time())
                data = (path, (ts, value))
                print(data)
                if time.time() - last_batch_ts >= BATCH_SEND_INTERVAL:
                    send_batch(batch)
                    batch = []
                    last_batch_ts = time.time()
                else:
                    batch.append(data)



  


if __name__ == '__main__':
    main()
