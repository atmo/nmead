#!/usr/bin/python3
import socket
import time

import pynmea2

TCP_IP = 'localhost'
TCP_PORT = 5005

GRAPHITE_IP = '127.0.0.1'
GRAPHITE_PORT = 22003


#  for i in 4 6 8 16 2; do echo "nmea.count $i `date +%s`" | nc -q0 127.0.0.1 2003; sleep 6; done
def netcat(host, port, content):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
    s.sendall(content.encode())
    s.shutdown(socket.SHUT_WR)
    s.close()

def main():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((TCP_IP, TCP_PORT))
    s.listen(1)
    conn, _ = s.accept()
    file_obj = conn.makefile()
    while True:
        for line in file_obj:
            try:
                print(line)
                sentence = pynmea2.parse(line)
            except pynmea2.ParseError:
                continue
            metric = sentence.__class__.__name__
            for f in sentence.fields:
                desc, name = f[0], f[1]
                data = "nmea.{metric}.{part} {value} {ts}\n"\
                            .format(metric=metric.lower(), part=name, 
                                value=getattr(sentence, name), ts=int(time.time()))
                print(data)
                netcat(GRAPHITE_IP, GRAPHITE_PORT, data)


  


if __name__ == '__main__':
    main()
