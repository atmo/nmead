#!/usr/bin/python3
import socket
import time

IP = 'localhost'
PORT = 5005
FILENAME = 'nmea.txt'


def main():
    reconnect = True
    s = None
    while True:
        if reconnect:
            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                s.connect((IP, PORT))
                reconnect = False
            except socket.error:
                continue
        with open(FILENAME, "rU") as f:
            for line in f:
                try:
                    s.send(line.encode())
                    time.sleep(0.1)
                    print(line)
                except socket.error:
                    reconnect = True
                    break

        print("reached EOFs")


if __name__ == '__main__':
    main()
